<?php
require __DIR__ . '/vendor/autoload.php';

$servername = "grw-sdb1.piratelabs.io";
//$servername = "localhost";


$username = "grw_b2c_read";
$password = "ZYmFcDLsE#43tvbWf";
$db = "grw_b2c";

/**
 * FIRST ORDER BY DAY
 */
$result_first = [];
$first_order = "SELECT post_date, FORMAT(SUM(wp_postmeta.meta_value),2) AS order_value FROM `wp_posts`
LEFT JOIN
wp_postmeta ON wp_posts.ID = wp_postmeta.post_id 
WHERE   
post_type = 'shop_order' AND 
post_status = 'wc-completed' AND
meta_key = '_order_total' AND
wp_posts.id IN (SELECT MIN(post_id) AS first_order FROM wp_posts
        INNER JOIN wp_postmeta ON wp_posts.id = wp_postmeta.post_id
        WHERE   
            post_type = 'shop_order' AND 
            post_status = 'wc-completed' AND
            meta_key = '_billing_email' 
        GROUP BY meta_value
        ORDER BY first_order
        )
GROUP BY CAST(post_date AS DATE)";

/**
 * AOV FIRST ORDER BY DAY
 */
$result_first_aov = [];
$first_order_aov = "SELECT post_date, FORMAT(AVG(wp_postmeta.meta_value),2) AS order_value FROM `wp_posts`
LEFT JOIN
wp_postmeta ON wp_posts.ID = wp_postmeta.post_id 
WHERE   
post_type = 'shop_order' AND 
post_status = 'wc-completed' AND
meta_key = '_order_total' AND
wp_posts.id IN (SELECT MIN(post_id) AS first_order FROM wp_posts
        INNER JOIN wp_postmeta ON wp_posts.id = wp_postmeta.post_id
        WHERE   
            post_type = 'shop_order' AND 
            post_status = 'wc-completed' AND
            meta_key = '_billing_email' 
        GROUP BY meta_value
        ORDER BY first_order
        )
GROUP BY CAST(post_date AS DATE)";


/**
 * NON-FIRST ORDER BY DAY
 */
$result_non_first = [];
$non_first_order = "SELECT post_date,FORMAT(SUM(wp_postmeta.meta_value),2) AS order_value FROM `wp_posts`
LEFT JOIN
wp_postmeta ON wp_posts.ID = wp_postmeta.post_id 
WHERE   
post_type = 'shop_order' AND 
post_status = 'wc-completed' AND
meta_key = '_order_total' AND
wp_posts.id NOT IN (SELECT MIN(post_id) AS first_order FROM wp_posts
        INNER JOIN wp_postmeta ON wp_posts.id = wp_postmeta.post_id
        WHERE   
            post_type = 'shop_order' AND 
            post_status = 'wc-completed' AND
            meta_key = '_billing_email' 
        GROUP BY meta_value
        ORDER BY first_order
        )
GROUP BY CAST(post_date AS DATE)";

/**
 * ACTIVE USERS BY DATE
 */
$result_actives = [];
$actives_select = "SELECT post_date, COUNT(id) AS buyers FROM `wp_posts`
LEFT JOIN
wp_postmeta ON wp_posts.ID = wp_postmeta.post_id 
WHERE
meta_value IN (SELECT user_email FROM wp_users) AND
meta_key = '_billing_email' AND
id IN (SELECT MAX(post_id) AS first_order FROM wp_posts
        INNER JOIN wp_postmeta ON wp_posts.id = wp_postmeta.post_id
        WHERE   
            post_type = 'shop_order' AND 
            post_status = 'wc-completed' AND
            meta_key = '_billing_email'
        GROUP BY meta_value
        ) 
GROUP BY CAST(post_date AS DATE)";

/**
 * ACTIVE USERS BY DATE /LAST 4MONTHS
 */
$unique_buyers_in_4m = 0;
$actives_select_4m = "SELECT COUNT(post_id) AS buyers FROM `wp_postmeta`
WHERE
meta_value IN (SELECT user_email FROM wp_users) AND
meta_key = '_billing_email' AND
post_id IN (SELECT MIN(post_id) AS first_order FROM wp_posts
        INNER JOIN wp_postmeta ON wp_posts.id = wp_postmeta.post_id
        WHERE   
            post_type = 'shop_order' AND 
            post_status = 'wc-completed' AND
            meta_key = '_billing_email' AND 
            post_date BETWEEN (CURRENT_DATE - INTERVAL 4 MONTH) AND CURRENT_DATE
        GROUP BY meta_value
        )";

$result_total = [];
try {
    $conn = new PDO("mysql:host=$servername;dbname=$db", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare($first_order_aov);
    $stmt->execute();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        array_push($result_first_aov, $row);
    }

    $stmt = $conn->prepare($first_order);
    $stmt->execute();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        array_push($result_first, $row);
    }

    $stmt = $conn->prepare($non_first_order);
    $stmt->execute();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        array_push($result_non_first, $row);
    }

    $stmt = $conn->prepare($actives_select);
    $stmt->execute();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        array_push($result_actives, $row);
    }

    $stmt = $conn->prepare($actives_select_4m);
    $stmt->execute();
    $unique_buyers_in_4m = $stmt->fetch(PDO::FETCH_ASSOC)['buyers'];

    foreach ($result_first as $key=>$item) {
        $result_total[$key]['post_date'] = $item['post_date'];
        $result_total[$key]['f_order_value'] = $item['order_value'];
        $val = 0;
        foreach ($result_non_first as $nkey=>$nitem) {
            if (date('Y-m-d', strtotime($item['post_date'])) == date('Y-m-d', strtotime($nitem['post_date']))) {
                $val = $nitem['order_value'];
                break;
            }
        }
        $result_total[$key]['nf_order_value'] = $val;

        $val = 0;
        foreach ($result_first_aov as $aitem) {
            if (date('Y-m-d', strtotime($item['post_date'])) == date('Y-m-d', strtotime($aitem['post_date']))) {
                $val = $aitem['order_value'];
                break;
            }
        }
        $result_total[$key]['f_aov'] = $val;

        $val = 0;
        foreach ($result_actives as $acitem) {
            if (date('Y-m-d', strtotime($item['post_date'])) == date('Y-m-d', strtotime($acitem['post_date']))) {
                $val = $acitem['buyers'];
                break;
            }
        }
        $result_total[$key]['buyers'] = $val;
    }
}
catch(PDOException $e)
{
    echo "Connection failed: " . $e->getMessage();
}

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Google Sheets API PHP Quickstart');
    $client->setScopes([Google_Service_Sheets::SPREADSHEETS]);
    $client->setAuthConfig('b2cconnect-10b03d98dccb.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    $tokenPath = 'token.json';
    if (file_exists($tokenPath)) {
        $accessToken = json_decode(file_get_contents($tokenPath), true);
        $client->setAccessToken($accessToken);
    }

    if ($client->isAccessTokenExpired()) {
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
            $client->setAccessToken($accessToken);

            if (array_key_exists('error', $accessToken)) {
                throw new Exception(join(', ', $accessToken));
            }
        }
        if (!file_exists(dirname($tokenPath))) {
            mkdir(dirname($tokenPath), 0700, true);
        }
        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
}


// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Sheets($client);
$spreadsheetId = '12CpN6rLo2hAXht5T_R93a7I51n7bp7u-93rfiJr8Cuo';
$sheets = new \Google_Service_Sheets($client);

$data = [];

// The first row contains the column titles, start pulling data from row 2
$currentRow = 2;
$currentDateRow = 0;

$range = 'A2:E';
$rows = $sheets->spreadsheets_values->get($spreadsheetId, $range, ['majorDimension' => 'ROWS']);

if (isset($rows['values'])) {
    $currentRow = count($rows);
    foreach ($result_total as $key=>$item) {
        if (date('Y-m-d', strtotime($rows['values'][$currentRow-1][0])) == date('Y-m-d', strtotime($item['post_date']))) {
            $currentDateRow = $key +1;
            break;
        }
    }
}

$requestCount = 0; // up to 100 allowed

for ($key = $currentDateRow; $key < count($result_total) -1; $key++) {
    // update date
    $updateRange = 'A'.$currentRow;
    $updateBody = new \Google_Service_Sheets_ValueRange([
        'range' => $updateRange,
        'majorDimension' => 'ROWS',
        'values' => ['values' => date('Y-m-d', strtotime($result_total[$key]["post_date"]))],
    ]);
    $sheets->spreadsheets_values->update(
        $spreadsheetId,
        $updateRange,
        $updateBody,
        ['valueInputOption' => 'USER_ENTERED']
    );

    // update fist orders
    $updateRange = 'B'.$currentRow;
    $updateBody = new \Google_Service_Sheets_ValueRange([
        'range' => $updateRange,
        'majorDimension' => 'ROWS',
        'values' => ['values' => $result_total[$key]["f_order_value"]],
    ]);
    $sheets->spreadsheets_values->update(
        $spreadsheetId,
        $updateRange,
        $updateBody,
        ['valueInputOption' => 'USER_ENTERED']
    );

    // update non-first orders
    $updateRange = 'C'.$currentRow;
    $updateBody = new \Google_Service_Sheets_ValueRange([
        'range' => $updateRange,
        'majorDimension' => 'ROWS',
        'values' => ['values' => $result_total[$key]["nf_order_value"]],
    ]);
    $sheets->spreadsheets_values->update(
        $spreadsheetId,
        $updateRange,
        $updateBody,
        ['valueInputOption' => 'USER_ENTERED']
    );

    // update first buyers aov
    $updateRange = 'D'.$currentRow;
    $updateBody = new \Google_Service_Sheets_ValueRange([
        'range' => $updateRange,
        'majorDimension' => 'ROWS',
        'values' => ['values' => $result_total[$key]["f_aov"]],
    ]);
    $sheets->spreadsheets_values->update(
        $spreadsheetId,
        $updateRange,
        $updateBody,
        ['valueInputOption' => 'USER_ENTERED']
    );

    // update active buyers
    $updateRange = 'E'.$currentRow;
    $updateBody = new \Google_Service_Sheets_ValueRange([
        'range' => $updateRange,
        'majorDimension' => 'ROWS',
        'values' => ['values' => $result_total[$key]["buyers"]],
    ]);
    $sheets->spreadsheets_values->update(
        $spreadsheetId,
        $updateRange,
        $updateBody,
        ['valueInputOption' => 'USER_ENTERED']
    );

    $currentRow++;
    $requestCount += 5;
    if($requestCount >=50) {
        sleep(100);
        $requestCount = 0;
    }
}

$updateBody = new \Google_Service_Sheets_ValueRange([
    'range' => 'F2',
    'majorDimension' => 'ROWS',
    'values' => ['values' => $unique_buyers_in_4m],
]);
$sheets->spreadsheets_values->update(
    $spreadsheetId,
    'F2',
    $updateBody,
    ['valueInputOption' => 'USER_ENTERED']
);

echo 'Done';
echo '<br>Unique logged buyers in last 4 months: '. $unique_buyers_in_4m;
echo '<form action="index.php" method="post">
    <input type="submit" name="update" value="Back" />
</form>';